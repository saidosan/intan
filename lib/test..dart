double addNumbers(num1, num2) {
  print(num1 + num2);
}

void main2() {
  print(addNumbers(1, 2));
  print(addNumbers(3, 3));
  print('Hello!');
}

////////////////////

void main() {
  double firstResult;
  firstResult = addNumbers(1,2.4); //infered
  var secondResult = addNumbers(3, 3);
  //...
  print(firstResult);
  print(secondResult);
  print('Hello!');
}